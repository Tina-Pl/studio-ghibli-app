# Studio Ghibli App
---
App that that displays all the species that appear in anime films made by a Japanese film studio - Studio Ghibli. It also allows you to search species.  
The main goal here was to work with a web API and retrieve data with Fetch API. I also added the search field to practice filtering data.  

## Built With
---
- HTML
- CSS
- Javascript
- [Studio Ghibli API](https://ghibliapi.herokuapp.com/) 