// Fetch Promise
let fetchPromise = fetch("https://ghibliapi.herokuapp.com/species");
//console.log(fetchPromise);

// Extract data from the response Promise object
fetchPromise.then(response => {
  // console.log(response);
  return response.json();
}).then(data => {
  //console.log(data);
  createSpeciesList(data);
});


// ====== createSpeciesList function ======
let container = document.getElementById('container');

// loop through data, create elements, display data
let createSpeciesList = (jsonData) => {
  for (let i = 0; i < jsonData.length; i++) {
    // create elements
    let article = document.createElement('article');
    let title = document.createElement('h2');
    let type = document.createElement('p');
    let eyes = document.createElement('p');
    let hair = document.createElement('p');

    // add species class name to articles
    let articleClass = (jsonData[i].classification).toLowerCase();
    article.classList.add(articleClass);
    
    // add text to created elements
    title.textContent = jsonData[i].name;
    type.textContent = `Species: ${jsonData[i].classification}`;
    eyes.textContent = `Eye color: ${jsonData[i].eye_colors}`;
    hair.textContent = `Hair color: ${jsonData[i].hair_colors}`;
    
    // append elements to article
    article.appendChild(title);
    article.appendChild(type);
    article.appendChild(eyes);
    article.appendChild(hair);

    // append article to container
    container.appendChild(article);
  }
}

// ====== Filtering data on search ======
let input = document.getElementById('search-field');
let btn = document.getElementById('search-btn');
let articles = document.getElementsByTagName('article');

  // ==== filterSpecies function ====
  let filterSpecies = () => {
    /* 1. Get input value and change it to lowercase, so that it matches
     article class name*/
    let inputValue = (input.value).toLowerCase();
    //console.log(inputValue);
    
    /* 2. Loop through all articles, handle empty input, remove those articles that don't 
      have the same class as the input value*/
    let allArticles = [...articles];
    allArticles.forEach(filterArticles);

    // == filterArticles function ==
    function filterArticles(item, index, arr){
      if (input.value === '') {
        // change placeholder and border color
        item.style.display = 'inline-block';
        input.setAttribute('placeholder', 'Please, enter species');
        input.style.borderBottomColor = '#faaca8';
        // on focus, change placeholder back to original
        input.addEventListener('focus', () => {
          input.setAttribute('placeholder', 'Search species ...');
        });
      } else {
        //remove articles whose class doesn't match input value
        if(!arr[index].classList.contains(inputValue)) {
          setTimeout(() => {
            item.style.display = 'none';
          }, 500);
        } 
        // On focus, clear input and display all articles again 
        input.addEventListener('focus', () => {
          input.value = '';
          setTimeout(() => {
            item.style.display = 'inline-block';
          }, 500);
        })
      }
    } 
  }

  // ==== call filterSpecies on button click ====
  btn.addEventListener('click', filterSpecies);
